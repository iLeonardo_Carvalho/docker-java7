![Java Logo](https://lh3.googleusercontent.com/Z7kwGHmabsWiKnZ4bnFDtdr0fq1gfLVG6tfAbHdVwiLl1BQeDDOjB1NeOPI_marO-DQBsIshNT2_40qfvOPn0AsZFetcMzwQdQ)




Docker Java/v7
===================

Este container contém contêm a configuração do **Java 7u80** que foi publicado em 14 de Abril de 2015 ([Java Releases](https://java.com/en/download/faq/release_dates.xml)), para criar outros contêineres que necessita do PATH **%JAVA_HOME%** e disponível via repositório do Ubuntu graças ao ["WebUpd8" team](https://launchpad.net/~webupd8team).

> OBS.:
>
> Devido às modificações durante o [Docker CHANGELOG](https://github.com/docker/docker/blob/master/CHANGELOG.md), foi aplicado uma padronização de como construir o Dockerfile e não repetir códigos e tentar minimizar o uso do RUN (Runtime), pois na versão 0.7.2 (16/12/2013) não pode utrapassar mais de 27 image depth.

------



[TOC]

## Requisitos

Foi utilizado a ultíma publicação do Java v7 **Release**, acessado pelo site [Oficial da Oracle](http://www.oracle.com/technetwork/pt/java/javase/downloads/jre7-downloads-1880261.html) no dia **22 de dezembro de 2016**, sendo que a Oracle não dá mais suporte em relação de falhas e bugs.

> **Notas:**

> - [UBUNTU](#UBUNTU): v16.04 (Xenial) com Modificações visiveis no Dockerfile no [ileonardo/base-ubuntu](https://hub.docker.com/r/ileonardo/base-ubuntu/)

> **Obs.:**

> - Foi instalado os seguintes pacotes essenciais:
>
>
> - [ca-certificates](#ca-certificates): Certificate Authority (CA)
>
>   Os comando **ADD** pode substituir o **wget/curl** e o **software-properties-common** além de trazer muita biblioteca para ser executado, aumenta muito o tamanho do contêiner e recomendo visitar o site [launchpad](https://launchpad.net/) que traz um meio manual de adicionar o repositório com as keys necessários do pacote que deseja instalar.

----------


Comandos Básicos
-------------------

#### Uso

##### Utilize os Comandos Abaixo

Para construir a partir do Código Fonte:

```
docker build -t ileonardo/java7 .
```

> **Obs:** O ponto final significa que o **Dockerfile** esta na pasta atual.

Ou se preferir baixar pelo [Docker Hub](https://hub.docker.com/explore/):

```
docker pull ileonardo/java7
```

Para Executar:

```
docker run -it ileonardo/java7
```

Ou:

```
docker run -dit ileonardo/java7
```

Entrar no Container em daemon:

```
docker exec -it <ID_Container> /bin/bash
```

Sair sem encerrar o Container:

```
[CTRL] p + q
```

#### Essenciais

Ver todas as Imagens no **HOST**:

```
docker images
```

Ver todos os Containers no **HOST**:

```
docker ps -a
```

Remover uma Imagem no **HOST**:

```
docker rmi <nome_imagem>
```

Remover um Container no **HOST**:

```
docker rm <Nome_Container>
```

Remover *dangling images* (Imagens sem TAG, quer dizer quando rodou o **Dockerfile** que falhou ele cria uma imagem <none>)

```
docker rmi -f $(docker images | grep "<none>" | awk "{print \$3}")
```

Remover o histórico dos comandos do Container no **HOST**:

```
docker stop $(docker ps -a -q) && docker rm $(docker ps -a -q)
```

Remover Todas às Imagens e Containers no **HOST**:

```
docker stop $(docker ps -a -q) && \
docker rm $(docker ps -a -q) && \
docker rmi $(docker images -q)
```

#### Manipulação de Dados

Copiar um arquivo do Container para o **HOST**:

```
docker cp <ID_Container>:/caminho/no/container/arquivo /caminho/no/host
```

Copiar um arquivo do **HOST** para o Container:

```
docker cp /caminho/no/host/arquivo <ID_Container>:/caminho/no/container
```

#### Monitoramento de Containers

Para ver as estatísticas de um Container específico no **HOST**:

```
docker stats <Nome_container>
```

Para ver as estatísticas de **todos** Containers no **HOST**:

```
docker stats `docker ps | tail -n+2 | awk '{ print $NF }'`
```


Direitos autorais e Licença
-------------

Este trabalho não foi modificado de seus Criadores (Link's de consulta abaixo), foi adaptado de acordo com a documentação do mesmo e dando os créditos contida neste repositório, a busca e a organização para futuras atualizações deve ser dado ao **contributors.txt** (BY).

Este trabalho foi escrito por Leonardo Cavalcante Carvalho e está licenciado com uma [Licença **Apache-2.0**](https://www.apache.org/licenses/LICENSE-2.0).

[^stackedit]: [StackEdit](https://stackedit.io/) is a full-featured, open-source Markdown editor based on PageDown, the Markdown library used by Stack Overflow and the other Stack Exchange sites.
[^docker]: A instalação foi utilizado no domínio do [Docker](https://get.docker.com/) que está contido o script auto installer.
[^Mundo Docker]: Configuração e a descrição do comando documentada baseado nos tutoriais do [Mundo Docker](www.mundodocker.com.br).
[^Web UPD8]: Tutorial de como instalar o Oracle Java 8 no Ubuntu, disponível no site por [Andrew](http://www.webupd8.org/2012/09/install-oracle-java-8-in-ubuntu-via-ppa.html).